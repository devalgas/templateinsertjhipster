
entity Customer {
firstName String required,
lastName String required
gender Gender required,
email String required pattern(/^[^@\s]+@[^@\s]+\.[^@\s]+$/),
phone String required,
addressLine1 String required,
addressLine2 String,
city String required,
country String required
}

enum Gender {
MALE, FEMALE, OTHER
}

enum GalleryType {
PRIVATE, PUBLIC
}

entity Gallery {
    title String required,
    description TextBlob,
    picture ImageBlob,
    pictureUrl String,
    date ZonedDateTime,
	type GalleryType
}

entity Picture {
    title String required,
    description TextBlob,
    picture ImageBlob,
    pictureUrl String,
    date ZonedDateTime
}

relationship OneToOne {
Customer{user} to User
}

relationship ManyToMany {
	Gallery{picture(name)} to Picture{gallery}
}

relationship ManyToMany {
	Customer{gallery} to Gallery{customer}
}

// Set pagination options
paginate * with pagination

// Use Data Transfer Objects (DTO)
// dto * with mapstruct

// Set service options to all except few
service all with serviceImpl

// Set an angular suffix
// angularSuffix * with mySuffix
